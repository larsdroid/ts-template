# ts-template

A template to be used as a **TypeScript web application**.

## Included in this template

* NPM configuration (basically, the contents of our old trusty `package.json`)
* TypeScript configuration (configured in `tsconfig.json`)
* Vitest setup (tests are executed using `npm test`)
* Vite configuration (with an HTML template and an SVG)
